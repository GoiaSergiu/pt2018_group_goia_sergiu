package Controller;

import Model.DataExceptions;
import Model.*;
import View.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    private Form m_view;

    public Controller(Form view)
    {
        m_view=view;
        view.addAdunListener(new Adunare());
        view.addDifListener(new Scadere());
        view.addInmListener(new Inmultire());
        view.addDerivListener(new Derivare());
        view.addStergListener(new Stergetot());
        view.addInteListener(new Integrare());
        view.addImpListener(new Impartire());
    }
    class Adunare implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                Po a = new Po(m_view.get1pol());
                Po b = new Po(m_view.get2pol());
                Po c = Po.sum(a, b);
                m_view.Afis(c.toString());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }
        }
    }
    class Scadere implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                Po a = new Po(m_view.get1pol());
                Po b = new Po(m_view.get2pol());
                Po c = Po.dif(a, b);
                m_view.Afis(c.toString());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }
        }
    }
    class Inmultire implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                Po a = new Po(m_view.get1pol());
                Po b = new Po(m_view.get2pol());
                Po c = Po.product(a, b);
                m_view.Afis(c.toString());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }
        }
    }
    class Impartire implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                Po a = new Po(m_view.get1pol());
                Po b = new Po(m_view.get2pol());
                Po c = Po.division(a, b, true);
                Po r = Po.division(a, b, false);
                m_view.Afis(c.toStringDouble() + "\n" + "With a reaminder of :" + "\n" + r.toStringDouble());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }
            catch (OperationExceptions er2)
            {
                JOptionPane.showMessageDialog(null,er2.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();
            }
        }
    }
    class Derivare implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                Po a = new Po(m_view.get1pol());
                Po c = a.derivate();
                m_view.Afis(c.toString());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }
        }
    }
    class Integrare implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                Po a = new Po(m_view.get1pol());
                Po b = a.integrate();
                m_view.Afis(b.toStringDouble());
            }
            catch (DataExceptions er1)
            {
                JOptionPane.showMessageDialog(null,er1.getMessage());
                m_view.st1pol();
                m_view.st2pol();
                m_view.strez();

            }

        }
    }
    class Stergetot implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            m_view.st1pol();
            m_view.st2pol();
            m_view.strez();
        }
    }
}

