package MVC;

import Controller.Controller;
import View.Form;

public class Main {

    public static void main(String[] args) {
        Form f = new Form();
        Controller c = new Controller(f);
    }
}
