package Model;

public class DataExceptions extends Exception{
    public DataExceptions(String message)
    {
        super(message);
    }
}
