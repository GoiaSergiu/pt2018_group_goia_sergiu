package Model;

public class Mo {
    private int gr;
    private Number coef;

    public Mo()
    {}
    public Mo(int gr, int coef)
    {
        this.coef=coef;
        this.gr=gr;
    }
    public Mo(int gr, double coef)
    {
        this.gr=gr;
        this.coef=coef;
    }
    public int getGr() {
        return gr;
    }
    public double getCoefdouble()
    {
        return coef.doubleValue();
    }
    public void setGr(int gr) {
        this.gr = gr;
    }

    public int getCoef() {
        return coef.intValue();
    }

    public void setCoef(double coef) {
        this.coef = coef;
    }
    public static Mo product(Mo m, Mo n)
    {
        Mo p = new Mo();
        p.setGr(n.getGr()+m.getGr());
        p.setCoef(n.getCoef()*m.getCoef());
        return p;
    }
    public static Mo sum(Mo a, Mo b)
    {
        if(a.gr==b.gr)
        {
            Mo c = new Mo(a.gr,a.coef.intValue()+b.coef.intValue());
            return c;
        }
        else return null;

    }
    public void inv()
    {
        this.setCoef(this.getCoef()*(-1));
    }
    public void sum(Mo a)
    {
        if(this.gr==a.gr)
            this.coef=this.coef.intValue()+a.coef.intValue();
    }
    public static Mo dif(Mo a, Mo b)
    {
        if(a.gr==b.gr)
        {
            Mo c = new Mo(a.gr,a.coef.intValue()-b.coef.intValue());
            return c;
        }
        else return null;
    }
    public void dif(Mo a)
    {
        if(this.gr==a.gr)
            this.coef=this.coef.intValue()-a.coef.intValue();
    }
    public Mo moIntegration()
    {
        int g = this.gr+1;
        Mo m = new Mo();
        m.setGr(g);
        m.setCoef((this.coef.doubleValue()/g));
        return m;
    }
}
