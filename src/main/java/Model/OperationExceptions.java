package Model;

public class OperationExceptions extends Exception{
    public OperationExceptions(String message)
    {
        super(message);
    }
}
