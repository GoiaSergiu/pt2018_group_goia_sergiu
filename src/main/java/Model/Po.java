package Model;

import java.util.ArrayList;
import java.util.List;

public class Po {
    private List<Mo> polinom = new ArrayList<Mo>();

    boolean tryPatseInt(String value)
    {
        try{
            Integer.parseInt(value);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    public Po()
    {}

    public Po(String p) throws DataExceptions
    {
            int[] coef = new int[50]; int[] put = new int[50]; int[] semne = new int[50]; int k = 0; int k1 = 0; int m; int m1; int j=0;
            String aux = new String(); String aux1 = new String(); String ne = new String();
            if(p.length()==0) throw new DataExceptions("Date introduse incorect !");
            String[] parts = p.split("x");
            for (int i = 0; i < parts.length; i++) ne = ne + parts[i];
            if (ne.charAt(0) != '-') ne = "+" + ne;
            ne = "0" + ne;
            for (int i = 0; i < ne.length(); i++) {
                if (ne.charAt(i) == '^') { aux = ne.substring(j, i);
                    if(tryPatseInt(aux)) coef[k] = Integer.parseInt(aux);
                    else throw new DataExceptions("Date introduse incorect !");
                    k++; j = i + 1; }
                if (ne.charAt(i) == '+') { aux = ne.substring(j, i);
                    if(tryPatseInt(aux)) put[k1] = Integer.parseInt(aux);
                    else throw new DataExceptions("Date introduse incorect !");
                    semne[k1] = 1; k1++; j = i + 1;
                }
                if (ne.charAt(i) == '-') { aux = ne.substring(j, i);
                    if(tryPatseInt(aux)) put[k1] = Integer.parseInt(aux);
                    else throw new DataExceptions("Date introduse incorect !");
                    semne[k1] = -1; k1++; j = i + 1;
                } }
            if(tryPatseInt(aux)) m = Integer.parseInt(aux);
            else throw new DataExceptions("Date introduse incorect !");
            aux1 = ne.substring(j, ne.length());
            if(tryPatseInt(aux1)) m1 = Integer.parseInt(aux1);
            else throw new DataExceptions("Date introduse incorect !");
            put[k1] = m1; coef[k] = m; k1++; k++;
            for (int i = 1; i < k; i++) {
                Mo u = new Mo(put[i], coef[i - 1] * semne[i - 1]); this.polinom.add(u); }
            this.align();
    }

    public static Po sum(Po p1, Po p2)
    {
        Po pol = new Po(); int i = 0; int j = 0; int i1 = p1.getPolinom().size(); int j1 = p2.getPolinom().size(); p1.align(); p2.align();
        while (i < i1 && j < j1) {
            if (p1.getPolinom().get(i).getGr() < p2.getPolinom().get(j).getGr()) {
                pol.getPolinom().add(p2.getPolinom().get(j));
                j++;
            } else if (p1.getPolinom().get(i).getGr() > p2.getPolinom().get(j).getGr()) {
                pol.getPolinom().add(p1.getPolinom().get(i));
                i++;
            } else {
                pol.getPolinom().add(Mo.sum(p1.getPolinom().get(i), p2.getPolinom().get(j)));
                i++; j++;
            }
        }
        while (i < i1) {
            pol.getPolinom().add(p1.getPolinom().get(i));
            i++;
        }
        while (j < j1) {
            pol.getPolinom().add(p2.getPolinom().get(j));
            j++;
        }
        for(int w=0;w<pol.getPolinom().size();w++)
            if(pol.getPolinom().get(w).getCoef()==0)
                pol.getPolinom().remove(w);
        pol.align();
        return pol;
    }

    public void align()
    {
        Mo aux1 = new Mo();
        Mo aux2 = new Mo();
        for(int i=0;i<this.getPolinom().size();i++) {
            for (int j = 0; j < this.getPolinom().size(); j++) {
                if (this.getPolinom().get(i).getGr() > this.getPolinom().get(j).getGr()) {
                    aux1 = this.getPolinom().get(i);
                    aux2 = this.getPolinom().get(j);
                    this.getPolinom().set(i, aux2);
                    this.getPolinom().set(j, aux1);
                }
            }
        }
        for(int i=0;i<this.getPolinom().size()-1;i++)
            if(this.getPolinom().get(i).getGr()==this.getPolinom().get(i+1).getGr())
                {
                    Mo m = new Mo(this.getPolinom().get(i).getGr(),this.getPolinom().get(i).getCoef()+this.getPolinom().get(i+1).getCoef());
                    this.getPolinom().set(i,m);
                    this.getPolinom().remove(i+1);
                }
        for(int w=0;w<this.getPolinom().size();w++)
            if(this.getPolinom().get(w).getCoef()==0)
                this.getPolinom().remove(w);
    }
    public String toString()
    {
        String s = new String();
        if(this.polinom.isEmpty())
            s="0";
        else {
            for (Mo a : polinom) {
                if (a.getCoef() >= 0)
                    s = s + "+" + a.getCoef() + "x^" + a.getGr();
                else
                    s = s + a.getCoef() + "x^" + a.getGr();
            }
        }
        return s;
    }

    public String toStringDouble()
    {
        String s = new String();
        if(this.polinom.isEmpty())
            s="0";
        {
            for (Mo a : polinom) {
                if (a.getCoefdouble() >= 0)
                    s = s + "+" + a.getCoefdouble() + "x^" + a.getGr();
                else s = s + a.getCoefdouble() + "x^" + a.getGr();
            }
        }
        return s;
    }

    public static Po dif(Po p1, Po p2)
    {
            int i=0; int j=0; int i1=p1.getPolinom().size(); int j1=p2.getPolinom().size(); Po pol = new Po(); p1.align(); p2.align();
            while(i<i1 && j<j1)
            {
                if(p1.getPolinom().get(i).getGr() < p2.getPolinom().get(j).getGr())
                {   p2.getPolinom().get(j).inv();
                    pol.getPolinom().add(p2.getPolinom().get(j));
                    j++; }
                else
                    if(p1.getPolinom().get(i).getGr() > p2.getPolinom().get(j).getGr())
                    {   p1.getPolinom().get(i);
                        pol.getPolinom().add(p1.getPolinom().get(i));
                        i++; }
                    else {  pol.getPolinom().add(Mo.dif(p1.getPolinom().get(i),p2.getPolinom().get(j)));
                            i++; j++; }
            }
            while(i<i1)
            { pol.getPolinom().add(p1.getPolinom().get(i)); i++;}
            while(j<j1)
            { p2.getPolinom().get(j).inv();
              pol.getPolinom().add(p2.getPolinom().get(j));
              j++;
            }
            for(int w=0;w<pol.getPolinom().size();w++)
                if(pol.getPolinom().get(w).getCoef()==0)
                    pol.getPolinom().remove(w);
            pol.align();
            return pol;
    }

    public Po integrate()
    {
        this.align();
        Po k = new Po();
        for(Mo m : this.polinom)
        {
            Mo n = m.moIntegration();
            k.getPolinom().add(n);
        }
        return k;
    }
    public Po derivate()
    {
        this.align();
        Po p = new Po();
        for(Mo m : this.polinom)
        {
            if(m.getGr()>0) {
                int w1 = m.getCoef()*m.getGr();
                m.setCoef(w1);
                int w2 = m.getGr()-1;
                m.setGr(w2);
                p.getPolinom().add(m);
            }
        }
        p.align();
        return p;
    }

    public static Po product(Po p1, Po p2)
    {
        int i1; int i2; int i11 = p1.getPolinom().size(); int i22 = p2.getPolinom().size(); Po p = new Po();
        for(i1=0;i1<i11;i1++)
        {
            for(i2=0;i2<i22;i2++)
            {
               p.getPolinom().add(Mo.product(p1.getPolinom().get(i1),p2.getPolinom().get(i2)));
            }
        }
        p.align();
        return p;
    }

    public static Po division(Po n, Po d,boolean w) throws OperationExceptions
    {
        n.align(); d.align();
        if(d.getPolinom().isEmpty()) throw new OperationExceptions("Operatia nu a reusit !");
        if((d.getPolinom().get(0).getGr()>=1 && n.getPolinom().get(0).getGr()>=d.getPolinom().get(0).getGr()))
        {
            Po n1 = n;
            Po cat = new Po();
            while(n1.getPolinom().get(0).getGr()>=d.getPolinom().get(0).getGr()) {
                double co = n1.getPolinom().get(0).getCoefdouble() / d.getPolinom().get(0).getCoefdouble();
                int gr = n1.getPolinom().get(0).getGr() - d.getPolinom().get(0).getGr();
                Mo m = new Mo(gr, co);
                cat.getPolinom().add(m);
                Po aux = new Po();
                for (Mo a : d.getPolinom()) {
                    Mo au = new Mo(m.getGr() + a.getGr(), m.getCoefdouble() * a.getCoef());
                    aux.getPolinom().add(au);
                }
                Po ram = Po.dif(n1, aux);
                n1 = ram;
                if(n1.getPolinom().size()==0) break;
            }
            cat.align(); n1.align();
            if(w==true) return cat;
            else return n1;
        }
        if(n.getPolinom().get(0).getGr()<d.getPolinom().get(0).getGr())
        {
            Po cat =new Po();Po res = n;
            if(w==true) return cat;
            else return res;
        }
        return null;
    }

    public List<Mo> getPolinom() {
        return polinom;
    }

    public void setPolinom(List<Mo> polinom) {
        this.polinom = polinom;
    }
}
