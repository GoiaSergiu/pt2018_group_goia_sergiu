package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class Form extends javax.swing.JFrame {
    private JFrame frame = new JFrame();
    private TextField t1 = new TextField();
    private TextField t2 = new TextField();
    private TextArea a1 = new TextArea();
    private JLabel l1 = new JLabel("Primul Polinom : ");
    private JLabel l2 = new JLabel("Al 2-lea Polinom : ");
    private JLabel l3 = new JLabel("Rezultat : ");
    private JPanel panel1 = new JPanel();
    private JPanel mainep = new JPanel();
    private JPanel buttonp = new JPanel();
    private JPanel s = new JPanel();
    private JPanel e = new JPanel();
    private JPanel w = new JPanel();
    private JPanel n = new JPanel();
    private Button b1 = new Button("Adunare");
    private Button b2 = new Button("Scadere");
    private Button b3 = new Button("Inmultire");
    private Button b4 = new Button("Impartire");
    private Button b5 = new Button("Derivare");
    private Button b6 = new Button("Integrare");
    private Button b7 = new Button("Sterge Tot");
    public Form()
    {
        initialize();
    }
    private void initialize()
    {   Dimension d = new Dimension(100,30);
        t1.setPreferredSize(d);
        t2.setPreferredSize(d);
        a1.setEditable(false);
        buttonp.setLayout(new FlowLayout());
        buttonp.add(b1);
        buttonp.add(b2);
        buttonp.add(b3);
        buttonp.add(b4);
        buttonp.add(b5);
        buttonp.add(b6);
        buttonp.add(b7);
        mainep.setLayout(new BoxLayout(mainep,BoxLayout.Y_AXIS));
        mainep.add(l1);
        mainep.add(t1);
        mainep.add(l2);
        mainep.add(t2);
        mainep.add(buttonp);
        mainep.add(l3);
        mainep.add(a1);
        panel1.setLayout(new BorderLayout(0,0));
        panel1.add(s,BorderLayout.SOUTH);
        panel1.add(n,BorderLayout.NORTH);
        panel1.add(e,BorderLayout.EAST);
        panel1.add(w,BorderLayout.WEST);
        panel1.add(mainep,BorderLayout.CENTER);
        frame.setBounds(100,100,730,489);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(panel1);
        frame.setVisible(true);
    }
    public String get1pol()
    {
        return t1.getText().trim();
    }
    public void st1pol()
    {
        t1.setText("");
    }
    public void st2pol()
    {
        t2.setText("");
    }
    public void strez()
    {
        a1.setText("");
    }
    public String get2pol()
    {
        return t2.getText().trim();
    }
    public void Afis(String b)
    {
        a1.setText(b);
    }
    public void addAdunListener(ActionListener ad)
    {
        b1.addActionListener(ad);
    }
    public void addDifListener(ActionListener sc)
    {
        b2.addActionListener(sc);
    }
    public void addInmListener(ActionListener inm)
    {
        b3.addActionListener(inm);
    }
    public void addImpListener(ActionListener imp)
    {
        b4.addActionListener(imp);
    }
    public void addDerivListener(ActionListener der)
    {
        b5.addActionListener(der);
    }
    public void addInteListener(ActionListener inte)
    {
        b6.addActionListener(inte);
    }
    public void addStergListener(ActionListener st)
    {
        b7.addActionListener(st);
    }
}
