import Model.DataExceptions;
import Model.OperationExceptions;
import Model.Po;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PoTest {

    private static int nrEx=0;
    private static int nrSucc=0;
    private Po p1;
    private Po p2;

    @BeforeClass
    public static void BefCl()
    {
        System.out.println("Incepe Testul");
    }
    @AfterClass
    public static void AftCl()
    {
        System.out.println("Numar teste efectuate " + nrEx + "\nDintre care " + nrSucc + " reusite ");
    }
    @Before
    public void Bef()
    {
        nrEx++;
        System.out.println("Incepe un test!");
    }
    @After
    public void Aft()
    {
        System.out.println("Sfarsit test!");
    }
    @org.junit.Test
    public void sum() throws DataExceptions {
        p1=new Po("2x^4-3x^2+7x^0");
        p2=new Po("5x^6-4x^4+10x^2-1x^6");
        Po p3 = Po.sum(p1,p2);
        assertNotNull(p3);
        assertEquals("+4x^6-2x^4+7x^2+7x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void sum1() throws DataExceptions {
        p1=new Po("3x^7-5x^2+4x^4-10x^0");
        p2=new Po("-4x^4+10x^5+2x^1+1x^0");
        Po p3 = Po.sum(p1,p2);
        assertNotNull(p3);
        assertEquals("+3x^7+10x^5-5x^2+2x^1-9x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void dif() throws DataExceptions {
        p1=new Po("5x^6-3x^5+2x^2-1x^0");
        p2=new Po("7x^6+1x^6-3x^3+2x^1+10x^0");
        Po p3 = Po.dif(p1,p2);
        assertNotNull(p3);
        assertEquals("-3x^6-3x^5+3x^3+2x^2-2x^1-11x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void dif1() throws DataExceptions {
        p1=new Po("3x^0-7x^4+2x^3-13x^6");
        p2=new Po("-2x^2+10x^0-3x^4");
        Po p3 = Po.dif(p1,p2);
        assertNotNull(p3);
        assertEquals("-13x^6-4x^4+2x^3+2x^2-7x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void integrate() throws DataExceptions {
        p1=new Po("4x^6-2x^4+7x^1-1x^0");
        Po p3 = p1.integrate();
        assertNotNull(p3);
        assertEquals("+0.5714285714285714x^7-0.4x^5+3.5x^2-1.0x^1",p3.toStringDouble());
        nrSucc++;
    }
    @org.junit.Test
    public void integrate1() throws DataExceptions {
        p1=new Po("2x^7-1x^4+2x^9-7x^7+3x^5");
        Po p3 = p1.integrate();
        assertNotNull(p3);
        assertEquals("+0.2x^10-0.625x^8+0.5x^6-0.2x^5",p3.toStringDouble());
        nrSucc++;
    }
    @org.junit.Test
    public void derivate() throws DataExceptions {
        p1=new Po("4x^6-2x^4+7x^1-1x^0");
        Po p3 = p1.derivate();
        assertNotNull(p3);
        assertEquals("+24x^5-8x^3+7x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void derivate1() throws DataExceptions {
        p1=new Po("10x^9-7x^7+3x^0-1x^4");
        Po p3 = p1.derivate();
        assertNotNull(p3);
        assertEquals("+90x^8-49x^6-4x^3",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void product() throws DataExceptions {
        p1=new Po("1x^1-1x^0");
        p2=new Po("1x^1+1x^0");
        Po p3 = Po.product(p1,p2);
        assertNotNull(p3);
        assertEquals("+1x^2-1x^0",p3.toString());
        nrSucc++;
    }
    @org.junit.Test
    public void product1() throws DataExceptions {
        p1=new Po("7x^2-3x^3+5x^4+1x^0");
        p2=new Po("4x^6-2x^2-5x^0");
        Po p3 = Po.product(p1,p2);
        assertNotNull(p3);
        assertEquals("+20x^10-12x^9+28x^8-6x^6+6x^5-39x^4+15x^3-37x^2-5x^0",p3.toString());
        nrSucc++;
    }

    @org.junit.Test
    public void division() throws OperationExceptions, DataExceptions {
        p1=new Po("1x^2+9x^1+14x^0");
        p2=new Po("1x^1+7x^0");
        Po p3 = Po.division(p1,p2,true);
        Po p4 = Po.division(p1,p2,false);
        assertNotNull(p3);
        assertNotNull(p4);
        assertEquals("+1.0x^1+2.0x^0",p3.toStringDouble());
        assertEquals("0",p4.toStringDouble());
        nrSucc++;
    }
    @org.junit.Test
    public void division1() throws OperationExceptions, DataExceptions {
        p1=new Po("3x^5-4x^3+2x^2-7x^0");
        p2=new Po("1x^2-3x^1");
        Po p3 = Po.division(p1,p2,true);
        Po p4 = Po.division(p1,p2,false);
        assertNotNull(p3);
        assertNotNull(p4);
        assertEquals("+3.0x^3+9.0x^2+23.0x^1+71.0x^0",p3.toStringDouble());
        assertEquals("+213.0x^1-7.0x^0",p4.toStringDouble());
        nrSucc++;
    }
}